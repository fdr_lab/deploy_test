FROM openjdk:21-jdk
COPY build/libs/deploy-test-*-SNAPSHOT.jar /app/deploy-test.jar
CMD ["java", "-jar", "/app/deploy-test.jar"]