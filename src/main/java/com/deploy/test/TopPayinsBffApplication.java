package com.deploy.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopPayinsBffApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopPayinsBffApplication.class, args);
	}

}
